"""
This script runs the Flask application using a development server.
"""

from os import environ
from features.exception_notifier import ExceptionNotifier
import argparse
import os
import traceback
import sys

# Note: take care PYTHONPATH is set correctly

if __name__ == "__main__":
    script_path = os.path.dirname(os.path.realpath(__file__))
    os.environ['WRKDIR'] = script_path

    if os.environ.get('PASSES', None):
        os.environ['PASSES'] = str(int(os.environ['PASSES']) + 1)
    else:
        os.environ['PASSES'] = "1"
    print("Pass ", os.environ['PASSES'])
    from visualization.index import app

    exception_notifier_ = ExceptionNotifier(password="your_password", email_adr="debugger@gmail.com",
                                            send_mail="youremailid@yourmail.com")

    argparser = argparse.ArgumentParser()
    argparser.add_argument("-docker_mode", action="store_true",
                           default=False, required=False)
    argparser.add_argument("-remote_debugger", action="store_true",
                           required=False, default=True)

    args = argparser.parse_args()
    HOST = environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(environ.get('SERVER_PORT', '19888'))
    except ValueError:
        PORT = 19888
    # run the server with a try catch, send the traceback via email to the specified account
    try:
        app.run_server(debug=True, host=HOST, port=PORT, use_reloader=False)
    except Exception as ex:
        msg = ''.join(traceback.format_exception(etype=type(ex), value=ex, tb=ex.__traceback__))
        print(msg)
        if args.remote_debugger:
            exception_notifier_.send_mail(msg)
